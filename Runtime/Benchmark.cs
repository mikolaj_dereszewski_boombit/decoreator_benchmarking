﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Benchmarking
{
    using Benchmarking.Measurements;
    using Coredian.States;

    public class Benchmark : MonoBehaviour
    {
        #region Properties

        private static Benchmark instance;

        private static bool _isRunning = false;

        [SerializeField]
        private List<BenchmarkingMeasurement> _benchmarkMeasurements;

        #endregion



        #region MonoBehaviour

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
        }

        private void Start()
        {
            StartCoroutine(BenchmarkMeasurements());
        }

        #endregion



        #region Coroutines

        public static IEnumerator RunBenchmarking<T>() where T : class, IState, new()
        {
            if (_isRunning)
                yield break;
            _isRunning = true;
            Core.States.SwitchState<BenchmarkingState>();
            while (_isRunning)
                yield return null;
            Core.States.SwitchState<T>();
        }

        private IEnumerator BenchmarkMeasurements()
        {
            float result = 0f;
            float time = Time.time;
            Debug.LogError("Started Benchmark");
            for (int i = 0; i < _benchmarkMeasurements.Count; i++)
            {
                yield return _benchmarkMeasurements[i].RunMeasurement(value => result = value);
                OnFinishedMeasurement(_benchmarkMeasurements[i].ParameterID, result);
                yield return _benchmarkMeasurements[i].Clean();
            }
            time = Time.time - time;
            Debug.LogError("Finished Benchmark in: " + time.ToString() + " seconds");
            _isRunning = false;
        }

        #endregion



        #region Events

        public event Action<string, float> FinishedMeasurement;
        private void OnFinishedMeasurement(string parameterID, float result)
        {
            Debug.LogError(parameterID + " result: " + result.ToString());
            if (FinishedMeasurement != null)
                FinishedMeasurement(parameterID, result);
        }

        #endregion
    }
}