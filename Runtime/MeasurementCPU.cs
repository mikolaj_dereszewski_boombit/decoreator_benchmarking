﻿using System;
using System.Collections;
using System.Threading;
using UnityEngine;

namespace Benchmarking.Measurements
{
    public class MeasurementCPU : BenchmarkingMeasurement
    {
        public override IEnumerator RunMeasurement(Action<float> result)
        {
            ulong totalTicks = 0;
            yield return StartCoroutine(EvaluateProcessor(value => totalTicks = value, true, _duration));

            if (result != null)
                result(totalTicks);
        }

        private IEnumerator EvaluateProcessor(Action<ulong> result, bool allThreads, float duration)
        {
            int threadCount = Mathf.Max(1, (allThreads ? SystemInfo.processorCount - 1 : 1));

            Debug.Log("Starting Processor Evaluation on threads: " + threadCount);
            ThreadTicks[] threads = new ThreadTicks[threadCount];
            for (int i = 0; i < threads.Length; i++)
                threads[i] = new ThreadTicks();

            yield return new WaitForSeconds(duration);

            ulong total = 0;
            for (int i = 0; i < threads.Length; i++)
                total += threads[i].Evaluate();
            Debug.Log(threadCount + " Thread Processor Evaluation Completed, score: " + total);

            if (result != null)
                result(total);
        }

        private class ThreadTicks
        {
            private ulong _ticks = 0;
            private Thread _thread;

            public ThreadTicks()
            {
                _thread = new Thread(CountTicks) { Priority = System.Threading.ThreadPriority.Normal };
                _thread.Start();
            }

            public ulong Evaluate()
            {
                _thread.Abort();
                return _ticks;
            }

            private void CountTicks()
            {
                while (true)
                    _ticks++;
            }
        }
    }
}