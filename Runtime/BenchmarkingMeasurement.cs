﻿using System;
using System.Collections;
using UnityEngine;

namespace Benchmarking.Measurements
{
    public interface IBenchmarkingMeasurement
    {
        string ParameterID { get; }
        float Duration { get; }
        IEnumerator RunMeasurement(Action<float> result);
        IEnumerator Clean();
    }

    public class BenchmarkingMeasurement : MonoBehaviour, IBenchmarkingMeasurement
    {
        public string ParameterID { get { return _parameterID; } }
        public float Duration { get { return _duration; } }

        [SerializeField]
        protected string _parameterID;
        [SerializeField]
        protected float _duration;

        public virtual IEnumerator RunMeasurement(Action<float> result)
        {
            yield return null;
        }

        public virtual IEnumerator Clean()
        {
            yield return null;
        }
    }
}