﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Benchmarking.Config
{
    using Coredian.Config;
    using System.IO;

    public class BenchmarkingConfig : ConfigNode
    {
        public const string ConfigKey = "Benchmarking";
        public const string DefaultNodeFileName = "Benchmarking Config";
        public static string PathToJSON = "Assets/Core/Decoreators/Benchmarking/Assets/defaultConfig.json";

        public override string Key { get { return ConfigKey; } }
        public override bool SupportsGoogleSheets { get { return false; } }

        public List<BenchmarkParameter> BenchmarkParameters = new List<BenchmarkParameter>();

#if UNITY_EDITOR
        private void OnEnable()
        {
            if (BenchmarkParameters.Count != 0)
                return;

            string json = File.ReadAllText(PathToJSON);
            BenchmarkParametersList parameters = JsonUtility.FromJson<BenchmarkParametersList>(json);
            if (parameters != null && !Application.isPlaying)
                BenchmarkParameters = parameters.BenchmarkParameters;
        }
#endif
    }

    [Serializable]
    public class BenchmarkParameter
    {
        public string ParameterID = "Parameter";
        public float MinimalThresholdValue = 0f, MaximalThresholdValue = 300;
        public bool AutoAdjustThresholds = false;
        public List<BenchmarkThreshold> ThresholdList = new List<BenchmarkThreshold>();
    }

    [Serializable]
    public class BenchmarkThreshold
    {
        public string ThresholdLabel = "Threshold";
        public int BottomPercent = 0;
        public int TopPercent = 25;
    }

    [Serializable]
    public class BenchmarkParametersList
    {
        public List<BenchmarkParameter> BenchmarkParameters = new List<BenchmarkParameter>();
    }
}
