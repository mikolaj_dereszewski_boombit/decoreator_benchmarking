﻿using UnityEngine;
using UnityEditor;

namespace Benchmarking
{
    using Coredian.Editor;
    using Config;
    using System.IO;

    public class BenchmarkingMenu
    {
        private const string MenuRoot = CoreMenu.MenuRoot + "/Benchmarking";

        [MenuItem(MenuRoot + "/Config", false, CoreMenu.ModulesSectionU)]
        public static void SelectOrCreateConfig()
        {
            CoreMenu.SelectOrCreate<BenchmarkingConfig>(BenchmarkingConfig.DefaultNodeFileName);
        }

        public static void GenerateDefaultConfigJSON(BenchmarkingConfig config)
        {
            BenchmarkParametersList parameters = new BenchmarkParametersList();
            parameters.BenchmarkParameters = config.BenchmarkParameters;

            string json = JsonUtility.ToJson(parameters);
            using (FileStream fs = new FileStream(BenchmarkingConfig.PathToJSON, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(json);
                    writer.Close();
                    writer.Dispose();
                }
                fs.Close();
                fs.Dispose();
            }
            AssetDatabase.Refresh();
        }
    }

}

