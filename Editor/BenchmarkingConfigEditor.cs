﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Benchmarking.Config
{
    using Coredian.Editor;
    using Coredian.Config.Editor;

    [CustomEditor(typeof(BenchmarkingConfig))]
    public class BenchmarkingConfigEditor : ConfigNodeEditor
    {
        private BenchmarkingConfig _target;
        private List<bool> _dropdownThresholds;



        #region Editor

        protected override void OnEnable()
        {
            base.OnEnable();

            _target = target as BenchmarkingConfig;
            if (_target.BenchmarkParameters == null)
                _target.BenchmarkParameters = new List<BenchmarkParameter>();

            _dropdownThresholds = new List<bool>();
            for (int i = 0; i < _target.BenchmarkParameters.Count; i++)
                _dropdownThresholds.Add(false);
        }

        protected override void DrawInspector()
        {
            base.DrawInspector();

            for (int i = 0; i < _target.BenchmarkParameters.Count; i++)
            {
                if (!DrawBenchmarkParameter(_target.BenchmarkParameters[i], i))
                {
                    _target.BenchmarkParameters.RemoveAt(i);
                    _dropdownThresholds.RemoveAt(i);
                    break;
                }
                GUILayout.Space(25);
            }

            if (GUILayout.Button("Add New Parameter"))
            {
                _target.BenchmarkParameters.Add(new BenchmarkParameter());
                _dropdownThresholds.Add(false);
            }
        }

        #endregion



        #region Drawers

        private bool DrawBenchmarkParameter(BenchmarkParameter param, int index)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);

            if (!DrawParameterProperties(param))
            {
                EditorGUILayout.EndVertical();
                return false;
            }

            GUILayout.Space(25);
            EditorGUI.indentLevel++;
            _dropdownThresholds[index] = EditorGUILayout.Foldout(_dropdownThresholds[index], "Parameter Thresholds", _dropdownThresholds[index] ? Styles.FoldoutActiveStyle : Styles.FoldoutInactiveStyle);
            EditorGUI.indentLevel--;
            if (_dropdownThresholds[index])
            {
                for (int i = 0; i < param.ThresholdList.Count; i++)
                {
                    if (!DrawParameterThreshold(param, i))
                    {
                        param.ThresholdList.RemoveAt(i);
                        break;
                    }
                    EditorGUILayout.Space();
                }

                if (GUILayout.Button("Add New Threshold"))
                    param.ThresholdList.Add(new BenchmarkThreshold());
            }

            EditorGUILayout.EndVertical();
            return true;
        }

        private bool DrawParameterProperties(BenchmarkParameter param)
        {
            EditorGUILayout.LabelField("Parameter name (ID):", Styles.HeaderLabel);
            EditorGUILayout.BeginHorizontal();
            param.ParameterID = EditorGUILayout.TextField(param.ParameterID);
            if (GUILayout.Button("Remove Parameter"))
            {
                EditorGUILayout.EndHorizontal();
                return false;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Auto Adjust Thresholds Range");
            param.AutoAdjustThresholds = EditorGUILayout.Toggle(param.AutoAdjustThresholds);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Measurements Value Range:", Styles.HeaderLabel);
            EditorGUILayout.BeginHorizontal();
            param.MinimalThresholdValue = EditorGUILayout.FloatField(param.MinimalThresholdValue, GUILayout.Width(100));
            EditorGUILayout.LabelField(" - ", GUILayout.Width(17));
            param.MaximalThresholdValue = EditorGUILayout.FloatField(param.MaximalThresholdValue, GUILayout.Width(100));
            EditorGUILayout.EndHorizontal();

            return true;
        }

        private bool DrawParameterThreshold(BenchmarkParameter param, int index)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.LabelField("Threshold Label:");
            EditorGUILayout.BeginHorizontal();
            param.ThresholdList[index].ThresholdLabel = EditorGUILayout.TextField(param.ThresholdList[index].ThresholdLabel);
            if (GUILayout.Button("Remove Threshold"))
            {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndVertical();
                return false;
            }
            EditorGUILayout.EndHorizontal();

            DrawPercentSlider(ref param.ThresholdList[index].BottomPercent, ref param.ThresholdList[index].TopPercent);
            if(param.AutoAdjustThresholds)
            {
                if (index > 0)
                    param.ThresholdList[index - 1].TopPercent = param.ThresholdList[index].BottomPercent;
                if (index < param.ThresholdList.Count - 1)
                    param.ThresholdList[index + 1].BottomPercent = param.ThresholdList[index].TopPercent;
            }

            EditorGUILayout.EndVertical();
            return true;
        }

        #endregion



        #region Utilities

        private void DrawPercentSlider(ref int bottom, ref int top)
        {
            float bottomTemp = bottom, topTemp = top;
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(bottom.ToString() + "%", GUILayout.Width(50));
            EditorGUI.MinMaxSlider(GUILayoutUtility.GetRect(20, 500, 20, 20), ref bottomTemp, ref topTemp, 0f, 100f);
            EditorGUILayout.LabelField(top.ToString() + "%", GUILayout.Width(50));

            bottom = (int)bottomTemp;
            top = (int)topTemp;

            EditorGUILayout.EndHorizontal();
        }

        #endregion
    }
}
